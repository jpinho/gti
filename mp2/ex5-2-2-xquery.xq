import module namespace istfx="tecnico:ulisboa:pt" at "istfx-library.xq";

let $uri-source := "http://feeds.dn.pt/DN-Ultimas",
    $uri-derived-source := "sampledoc-ex3.xml",
    $data := (
        let $doc-ex1 := istfx:get-newsfeed-bycat(doc($uri-source)),
            $doc-ex3 := doc($uri-derived-source)
            
        let $news-doc1 := $doc-ex1//item,
            $news-doc3 := (
                let $nl := "&#10;", $sp := "&#32;", $sp3 := ($sp, $sp, $sp)
                for $item in $doc-ex3/news/item
                return 
                    <item date="{
                            let $dt := xs:date(concat('2014','-',$item/date/month/text(),'-', $item/date/day/text())),
                                $month := $item/date/month/text(),
                                $day := $item/date/day/text(),
                                $week := istfx:day-of-week-abbrev-en($dt)
                            return 
                                concat($week, ', ', $day, ' ', istfx:month-abbrev-en($dt), ' 2014 00:00:00 GMT' )
                                
                        }" title="{ $item/title/text() }">
                        { $nl, $sp3, $item/title/text(), $nl, $sp }
                    </item>
            )
            
        return 
            <news>
                { $news-doc1 | $news-doc3 }
            </news>)

return istfx:get-totalnews-per-month($data)