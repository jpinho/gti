module namespace istfx = "tecnico:ulisboa:pt";
declare namespace p="http://www.parlamento.pt";

(::
 : Function requested in exercise 1-1.
 ::)
declare function istfx:get-newsfeed-bycat($feed) {
    let $out := (
        for $item in $feed//item
        let $cat := $item/category/text()
        group by $cat
        return 
            <category name="{ $item[1]/category/text() }"> 
            {
                for $cat_item in $item
                let $nl := "&#10;", $sp := "&#32;", $sp2 := ($sp,$sp), $sp3 := ($sp2, $sp)
                return
                    <item date="{ $cat_item/pubDate/text() }" 
                          title="{ $cat_item/title/text() }">
                        { $nl, $sp3, $cat_item/description/text(), $nl, $sp2 }
                    </item> 
            }
            </category>)
    return 
        <news>{$out}</news>
};

(::
 : Function requested in exercise 1-2.
 ::)
declare function istfx:get-related-news($news, $data, $n){  
    if ($n lt 0.0 or $n gt 1.0) then 
        error(Q{tecnico:ulisboa:pt:error}AttributeOutOfRange, 'Attribute $n must range between 0 and 1.')
    else (       
         <related-news> {
             for $session in $data//p:session
             return 
                 <session date="{ $session/@date }"> {
                     let $matches := (
                         for $newsinfo in $news//item,
                             $speech in $session/p:speech         
                             
                         let $news_title := $newsinfo/@title,
                             $news_text := $newsinfo/text(),
                             $speech_text := $speech/text()
                 
                         where istfx:check-similar-strings($speech_text, $news_title, $n) eq true()
                             or istfx:check-similar-strings($speech_text, $news_text, $n) eq true()
                         
                         return $newsinfo)

                     for $newsinfo in $matches
                     let $title := $newsinfo/@title
                     group by $title
                     return 
                         <item date="{ $newsinfo/@date }" title="{ $newsinfo/@title }" />  
                 }
                 </session>
         }
         </related-news>        
    )
};

(::
 : Function requested in exercise 1-3.
 ::)
declare function istfx:get-related-news-by-politician($news, $data){  
     <related-news> {
         for $session in $data//p:session
         return 
             <session date="{ $session/@date }"> {
                 let $matches := (
                     for $newsinfo in $news//item,
                         $speech in $session/p:speech         
                         
                     let $news_text := $newsinfo/text(),
                         $politician_name := $data//p:politician[@code = $speech/@politician]/text() 
             
                     where istfx:string-contains-permutation($news_text, $politician_name) eq true()
                     
                     return $newsinfo)

                 for $newsinfo in $matches
                 let $title := $newsinfo/@title
                 group by $title
                 return 
                     <item date="{ $newsinfo/@date }" title="{ $newsinfo/@title }" />  
             }
             </session>
     }
     </related-news>        
};

(::
 : Function requested in exercise 4-1
 ::)
declare function istfx:get-model($data, $speechs){
    let $politic-parties := distinct-values(
        for $pcode in distinct-values($speechs/@politician),
            $party in $data//p:politician[@code = $pcode]
        return $party/@party)
        
    , $parties := (        
        for $party in $politic-parties        
        let $spCount := count(
            for $sp in $speechs 
            where istfx:get-party($data, $sp) = $party
            return $sp)
        return <party name="{ $party }" size="{ $spCount }" />)
        
    , $tokens := 
        for $sp in $speechs,
            $token in distinct-values(for $t in tokenize(replace($sp/text(), "\.|,|!|\?|(&quot;)", ""), " ") return lower-case($t))          
        return 
            <word token="{ $token }">
                <speech party="{ istfx:get-party($data, $sp) }" />
            </word>

    return
        <model>
            { $parties }
            
            {
                for $word in $tokens
                let $token := $word/@token
                group by $token
                order by $token
                return 
                    <word token="{ $token }">
                    {
                        for $party in distinct-values($word/speech/@party)
                        order by $party
                        return 
                            <party name="{ $party }">
                                { count( $word/speech[@party = $party] ) }
                            </party>
                    }
                    </word>
            }
        </model>
};

(::
 : Function requested in exercise 4-2
 ::)
declare function istfx:get-party-of-speaker-nbayes ( $doc-speech as xs:string, $model) {
    let (: document - bag of words of a speech:)
        $words := tokenize($doc-speech, " "),
        
        (: the categories :)
        $parties := $model/party
    
    (: the combinations of the probability observing D (given a category [party]) with the prior probability of the category (party) :)
    let $p_Ci-D := 
        for $party in $parties
            return
                <prob party="{ $party/@name }" value="{
                    (: defined as the percentage of documents (speechs) that belong to category (party) Ci  :)
                    let $p_Ci := (
                            number($party/@size) 
                            div 
                            sum($parties/@size)),
                    
                        $p_D_Ci :=
                            istfx:sequence-product(
                                for $word in $words
                                
                                (: defined as the number of documents (speechs) in category (party) Ci that contain term (token) t :)
                                let $p_t-Ci :=  
                                    number($model/word[@token = $word]/party[@name = $party/@name]/text()) 
                                    div
                                    number($party/@size)
                    
                                return 
                                    number($p_t-Ci))
                    return
                        $p_D_Ci * $p_Ci
                                        
                }" />
    
    return
        data( $p_Ci-D[@value = max($p_Ci-D/@value)]/@party )
};

(::
 : Function requested in exercise 5-1
 ::)
declare function istfx:mediate-data-merge($uri-source as xs:string, $uri-derived-source as xs:string) {
    let $doc-ex1 := istfx:get-newsfeed-bycat(doc($uri-source)),
        $doc-ex3 := doc($uri-derived-source)
        
    let $news-doc1 := $doc-ex1//item,
        $news-doc3 := (
            let $nl := "&#10;", $sp := "&#32;", $sp3 := ($sp, $sp, $sp)
            for $item in $doc-ex3/news/item
            return 
                <item date="{
                        let $dt := xs:date(concat('2014','-',$item/date/month/text(),'-', $item/date/day/text())),
                            $month := $item/date/month/text(),
                            $day := $item/date/day/text(),
                            $week := istfx:day-of-week-abbrev-en($dt)
                        return 
                            concat($week, ', ', $day, ' ', istfx:month-abbrev-en($dt), ' 2014 00:00:00 GMT' )
                            
                    }" title="{ $item/title/text() }">
                    { $nl, $sp3, $item/title/text(), $nl, $sp }
                </item>
        )
        
    return 
        <news>
            { $news-doc1 | $news-doc3 }
        </news>
};

(::
 : Function requested in exercise 5-2-1
 ::)
declare function istfx:get-totalnews-per-month( $data ) {
    let $months :=
        for $item in $data/item
        let $date := data($item/@date),
            $month := istfx:get-month($date)
        return 
            <month value="{ $month }">
                { $item }
            </month>
    
    return 
        <news-count> {
            for $m in $months
            let $mvalue := $m/@value
            group by $mvalue
            order by $mvalue ascending
            return 
                <total month="{ $mvalue }">
                    { count($m/item) }
                </total>
        } </news-count>
};


(:: Auxiliary Functions ::)

(::
 : Given a string $x and $y, this function returns true of false, if their similarity ratio is greater or
 : equal than $leastCommonFactor. Case is ignored in the comparison of words between each of the 
 : strings.
 ::)
declare function istfx:check-similar-strings($x, $y, $leastCommonFactor) as xs:boolean {
    if ($leastCommonFactor lt 0.0 or $leastCommonFactor gt 1.0) then 
        error(Q{tecnico:ulisboa:pt:error}AttributeOutOfRange, 'Attribute $leastCommonFactor must range between 0 and 1.')
    else (    
        let $x_words := tokenize($x, '\s+'),
            $y_words := distinct-values(tokenize($y, '\s+')),
            $words_count := max((count(for $x in $x_words return $x), count(for $y in $y_words return $y))),
            $common_words_count := count(
                for $x_word in $x_words,
                    $y_word in $y_words
                where lower-case($x_word) eq lower-case($y_word)
                return $x_word )
                
        return ($common_words_count div $words_count) ge $leastCommonFactor
    )
};

(::
 : Given a string $test_str and a string $substring_to_test, this function returns true if any
 : permutation (composed by pairs of 2 words) of $substring_to_test exists in the string $test_str.
 ::)
declare function istfx:string-contains-permutation($test_str, $substring_to_test) as xs:boolean {
    let $substr_test_tokens := istfx:get-string-permutations-distinct($substring_to_test),
        $matches := (
            for $str_i in $substr_test_tokens
            where contains(lower-case($test_str), lower-case($str_i))
            return $str_i
        )
        
    return count($matches) gt 0
};

(:: 
 : Given a string $source_str, this function returns all possible permutations in pairs of 2 words, 
 : of the elements in that string. The elements of a string are splitted by the whitespace character, 
 : and the result contains only distinct permutations.
 ::)
declare function istfx:get-string-permutations-distinct($source_str){
    let $str_values := tokenize($source_str, '\s+'),
        $len as xs:integer := count($str_values),
        $permutations := (
            for $i in (1 to $len)
            return 
                if($i lt $len) then  (
                    concat( $str_values[$len], ' ', $str_values[$i]),
                    concat( $str_values[$i], ' ', $str_values[$len]),
                    (
                        for $k in (1 to $len)
                        return 
                            if($k ne $i) then
                                concat($str_values[$i], ' ', $str_values[$k])
                            else ()
                    )
                ) else ())
    return distinct-values($permutations)
};

declare function istfx:get-party($data, $speech){
    let $p := $data//p:politician[@code = $speech/@politician]
    return $p/@party
};

declare function istfx:sequence-product($s) {
    if (count($s) <= 1) then $s[1] else $s[1] * istfx:sequence-product($s[position()>1])
};

declare function istfx:month-abbrev-en ( $date as xs:anyAtomicType? )  as xs:string? {
   ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')
    [month-from-date(xs:date($date))]
};

declare function istfx:day-of-week-abbrev-en ( $date as xs:anyAtomicType? )  as xs:string? {
   ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat')
   [istfx:day-of-week($date) + 1]
};

declare function istfx:day-of-week ( $date as xs:anyAtomicType? )  as xs:integer? {

  if (empty($date))
  then ()
  else xs:integer((xs:date($date) - xs:date('1901-01-06'))
          div xs:dayTimeDuration('P1D')) mod 7
};

declare function istfx:get-month( $fulldate as xs:string ) as xs:integer {        
    if(contains($fulldate, "Jan")) then
        1
    else if(contains($fulldate, "Fev")) then
        2
    else if(contains($fulldate, "Mar")) then
        3
    else if(contains($fulldate, "Apr")) then
        4
    else if(contains($fulldate, "May")) then
        5
    else if(contains($fulldate, "Jun")) then
        6
    else if(contains($fulldate, "Jul")) then
        7
    else if(contains($fulldate, "Ago")) then
        8
    else if(contains($fulldate, "Sep")) then
        9
    else if(contains($fulldate, "Oct")) then
        10
    else if(contains($fulldate, "Nov")) then
        11
    else if(contains($fulldate, "Dez")) then
        12
    else 
        -1
};