import module namespace istfx="tecnico:ulisboa:pt" at "istfx-library.xq";

let $data := istfx:mediate-data-merge("http://feeds.dn.pt/DN-Ultimas", "sampledoc-ex3.xml")
return istfx:get-totalnews-per-month($data)