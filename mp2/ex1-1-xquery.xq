import module namespace istfx="tecnico:ulisboa:pt" at "istfx-library.xq";

declare variable $news-feed := doc("http://feeds.dn.pt/DN-Ultimas");
istfx:get-newsfeed-bycat($news-feed)

(: [Note for evaluator] - All functions are bundled in istfx-library.xq, including auxiliary functions. :)