import module namespace istfx="tecnico:ulisboa:pt" at "istfx-library.xq";
declare namespace p="http://www.parlamento.pt";

declare variable $parliament-data := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/1411154454773835/parliament-data-small.xml");

declare variable $speechs := $parliament-data//p:speech;
declare variable $model := istfx:get-model($parliament-data, $speechs);
declare variable $doc := "intervention from a politician";

let $party-of-speaker := istfx:get-party-of-speaker-nbayes($doc, $model) 
return $party-of-speaker