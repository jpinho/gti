import module namespace istfx="tecnico:ulisboa:pt" at "istfx-library.xq";

declare variable $parliament-data := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/1411154454773835/parliament-data-small.xml");
declare variable $parliament-big-data := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/566729524645336/parliament-data.xml");
declare variable $news-feed := doc("http://feeds.dn.pt/DN-Ultimas");
declare variable $percent := 0.4;

(: function test assessement :)
(: istfx:check-similar-strings("ola planeta terra", "ola Planeta marte terra planeta", 0.5) :)

istfx:get-related-news( istfx:get-newsfeed-bycat($news-feed), $parliament-big-data, $percent )

(: [Note for evaluator] - All functions are bundled in istfx-library.xq, including auxiliary functions. :)