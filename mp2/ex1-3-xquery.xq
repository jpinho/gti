import module namespace istfx="tecnico:ulisboa:pt" at "istfx-library.xq";

declare variable $parliament-data := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/1411154454773835/parliament-data-small.xml");
declare variable $parliament-big-data := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/566729524645336/parliament-data.xml");
declare variable $news-feed := doc("http://feeds.dn.pt/DN-Ultimas");

(: function test assessement :)
(: istfx:string-contains-permutation("Pedro Passos Coelho Maria", "o coelhinho saltou por cima da cartola do coelho passos coelho!") :)

istfx:get-related-news-by-politician( istfx:get-newsfeed-bycat($news-feed), $parliament-big-data )

(: [Note for evaluator] - All functions are bundled in istfx-library.xq, including auxiliary functions. :)