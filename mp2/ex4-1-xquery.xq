import module namespace istfx="tecnico:ulisboa:pt" at "istfx-library.xq";
declare namespace p="http://www.parlamento.pt";

declare variable $parliament-data := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/1411154454773835/parliament-data-small.xml");
declare variable $parliament-big-data := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/566729524645336/parliament-data.xml");
declare variable $speechs := $parliament-data//p:speech;

istfx:get-model($parliament-data, $speechs)