(: exercise 1.1 :)
declare variable $doc := doc("https://dspace.ist.utl.pt/bitstream/2295/1115872/1/AirFlightsData.xml")/doc;

declare variable $romeAirId := {
    for $airport in $doc/Airport
    where $airport/name/text() = "Rome Fiumicino"
    return data($airport/@airId)
};

for $flight in $doc/Flight
where $flight/destination/text() = $romeAirId
return { $flight }

(: exercise 1.2 :)

declare variable $doc := doc("https://dspace.ist.utl.pt/bitstream/2295/1115872/1/AirFlightsData.xml")/doc;

declare variable $romeAirId := {
    for $airport in $doc/Airport
    where fn:contains($airport/name/text(), "Rome")
    return data($airport/@airId)
};

for $flight in $doc/Flight
where $flight/destination/text() = $romeAirId
order by $flight/departure
return { $flight }

(: exercise 1.3 :)

declare variable $doc := doc("https://dspace.ist.utl.pt/bitstream/2295/1115872/1/AirFlightsData.xml")/doc;

fn:distinct-values(
    for $pass in $doc/Passenger
    where $pass/passportnumber/text() = $doc/Reservation/passRef/text()
    return { $pass/name/text() }
)