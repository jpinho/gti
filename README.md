### GTI Project

Academic Project about Data Management and Transformation.

### Wiki

[Checkout our awesome Wiki !!!](https://bitbucket.org/jpinho/gti/wiki/Home)

### Quick Links

* [Project Issues](https://bitbucket.org/jpinho/gti/issues?sort=id)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Commit Discipline](https://bitbucket.org/jpinho/gti/wiki/Commit%20Discipline)
* [OSX Tools for XSLT 'n XSD OSX](http://www.soliantconsulting.com/blog/2009/02/tricks-from-the-command-line-xsltproc-and-xmllint)
* [Use Issue Tracker when Committing](https://confluence.atlassian.com/display/BITBUCKET/Resolve+issues+automatically+when+users+push+code)
* [XQuery Update by xqilla](http://xqilla.sourceforge.net/XQueryUpdate)
* [XQuery Update by basex](http://docs.basex.org/wiki/XQuery_Update#delete)

### Overview ###
* * *
[TOC]

### Structure ###

##### Folders

* Folder: "samples": contains files usefull as documentation.

##### Dependecies

* Zorba

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Team ###

* Repo owner: [João Pinho](mailto:jpe.pinho@gmail.com)
* Team members: [Miguel Franco](mailto:miguelbfranco@gmail.com)

### References

* SitePoint [URL] http://www.sitepoint.com/xml-dtds-xml-schema