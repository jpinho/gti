(: note the exercise have a tranformation copy-modify-return to allow preview of the modifications :)

declare namespace p="http://www.parlamento.pt";
declare variable $docx := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/1411154454773835/parliament-data-small.xml");

(: #start# remove this - not needed :)
copy $doc := $docx
modify ( 
(: #end# remove this - not needed :)
	
    for $sx at $i in $doc//p:session
    let $frequencies_query :=
      for $i in (
        for $s in (
            let $p := 
              for $speech in $doc//p:speech
              let $party :=data($doc//p:politician[@code=$speech/@politician]/@party)
              let $session := $speech/parent::node()
              group by $session
              return 
                  <session>
                    { for $p in $party group by $p return <speech party='{$p}' 
                      count='{ count(for $k in $party where $k=$p return $k) }'></speech>} 
                  </session>
            return 
              $p
         )
        let $pp := $s/p:speech/@party
        group by $pp
        return $s)
        
      return 
        <session most-frequent='{ $i/speech[@count = max($i/speech/@count)][1]/@party }' />
    return
      ( 
        insert node (attribute most-frequent { $frequencies_query[$i]/@most-frequent }) as last into $sx,
        
		(: isto é uma padeirísse, nós sabemos, mas como é pedido dentro da mesma query, não encontrá-mos alternativa :)
        if($i = 1) then (
          for $politician in $doc//p:politician    
          let $numinterv := count($doc//p:session/p:speech[@politician = $politician/@code])
          let $numsessions := count($doc//p:session[p:speech/@politician = $politician/@code])
          return (
              insert node (attribute {'num-sessions'} { $numsessions }) as last into $politician,
              insert node (attribute {'num-interventions'} { $numinterv }) as last into $politician
          )
        ) else ()
      )
	  
(: #start# remove this - not needed :)
)
return $doc
(: #end# remove this - not needed :)
  
  