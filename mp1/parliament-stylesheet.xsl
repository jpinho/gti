<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:poli="http://www.parlamento.pt">
	<xsl:output method="html" indent="yes" version="5.0" omit-xml-declaration="yes" encoding="iso-8859-1" standalone="yes"/>
		
	<xsl:template match="/">
		<html>
			<body>
				<h2>Parliament Politicians</h2>
				<xsl:apply-templates select="poli:parliament" />
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="poli:parliament">
		<xsl:if test="count(//poli:politician) &gt; 0">
			<table border="1" style="border-collapse:collapse;" cellMargin="0" cellPadding="5px">
				<thead>
					<tr style="background-color: #EEE;">
						<xsl:for-each select="//poli:politician[1]/@*">
							<td style="text-transform: capitalize;">
								<xsl:value-of select="name()" />
							</td>
						</xsl:for-each>
						<td>Name</td>
						<td>Nº Interventions</td>
						<td>Nº Sessions</td>
					</tr>
				</thead>
				<tbody>
					<xsl:apply-templates select="poli:politicians/poli:politician" />
				</tbody>
			</table>
		</xsl:if>
		<xsl:if test="count(//poli:politician) &lt; 1">
			<span> ** Upsss! There are no politicians on the given XML ** </span>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="poli:politician">
		<xsl:variable name="code" select="@code"/>
		<tr>
			<xsl:for-each select="@*">
				<td>
					<xsl:value-of select="." />
				</td>
			</xsl:for-each>
			<td><xsl:value-of select="."/></td>
			<td><xsl:value-of select="count(//poli:parliament-interventions/poli:session/poli:speech[@politician=$code])" /></td>
			<td><xsl:value-of select="count(//poli:parliament-interventions/poli:session[speech/@politician=$code])" /></td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
