declare namespace p="http://www.parlamento.pt";
declare variable $doc := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/1411154454773835/parliament-data-small.xml");

declare variable $word_spaces_pattern := "[\^ ]+";

<interventions>
    {
        for $interv at $i in subsequence((
            for $session in $doc//p:session
            return {
                for $speech in $session/p:speech
                order by count(tokenize($speech/text(), $word_spaces_pattern)) descending
                return 
                    <intervention 
                        word-count="{ count(tokenize($speech/text(), $word_spaces_pattern)) }" 
                        session-date="{ $session/@date }" 
                        politician="{ $doc//p:politician[@code = $speech/@politician]/text() }" 
                        party="{ $doc//p:politician[@code = $speech/@politician]/@party }">
                        { $speech/text() }
                    </intervention>
            }), 1, 5)
        return 
            <intervention
                session-date="{ $interv/@session-date }"
                party="{ $interv/@party }"
                politician="{ $interv/@politician }"
                rank-order="{ $i }">
                { $interv/text() }        
            </intervention>
    }
</interventions>
