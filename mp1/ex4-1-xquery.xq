declare namespace p="http://www.parlamento.pt";
declare variable $doc := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/1411154454773835/parliament-data-small.xml");

declare variable $repliers :=
    for $speech in $doc//p:speech
    where $speech[@politician = data($doc//p:politician[text() = "Pedro Passos Coelho"]/@code)]
    return {
        $speech/following-sibling::p:speech[1]
    };

for $replier in distinct-values(data($repliers/@politician))
where count($repliers[@politician = $replier]) >= 2
return $doc//p:politician[@code = $replier]/text()
