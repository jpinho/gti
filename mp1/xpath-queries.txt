<!-- xPath Queries -->

#1
count(distinct-values(//session[number(translate(@date,'-','')) >= 20140301 and number(translate(@date,'-','')) <= 20140331]/speech/@politician))

#2
//politician[@code=//speech[contains(., "ensino superior") or contains(., "educação")]/@politician]

#3
//politician[@code = //speech[@politician=//politician[. = "José Seguro"]/@code]/following-sibling::speech[not(@politician=//politician[. = "José Seguro"]/@code)]/@politician]

#4
avg(//politician[@code = //session[number(translate(@date,'-','')) = 20140301]/speech[@politician = //politician[@party = 'PSD']/@code ][1]/following-sibling::speech/@politician]/@age)