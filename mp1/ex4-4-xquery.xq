(: note the exercise have a tranformation copy-modify-return to allow preview of the modifications :)

declare namespace p="http://www.parlamento.pt";
declare variable $docx := doc("https://fenix.tecnico.ulisboa.pt/downloadFile/1411154454773835/parliament-data-small.xml");

(: #start# remove this - not needed :)
copy $doc := $docx
modify ( 
(: #end# remove this - not needed :)
    
  let $poliToRemove := 
    for $politician in $doc//p:politician
    where count($doc//p:speech[@politician = $politician/@code]) < 3
    return $politician

  let $spToRemove := 
    for $speech in $doc//p:speech
    where $speech/@politician = $poliToRemove/@code
    return $speech

  return 
    (
      delete nodes $poliToRemove, 
      delete nodes $spToRemove
    )

(: #start# remove this - not needed :)
) 
return $doc
(: #end# remove this - not needed :)